import React, { useState, useEffect } from "react";
import genresObj from "../movies-data";
import "./MovieList.css";
import { useNavigate } from "react-router-dom";
import Header from "../Header/Header";

function MovieList() {

  const [state, setstate] = useState(genresObj);
  const [searchMovies, setSearchMovies] = useState('');

  let navigate = useNavigate();
  console.log(state);

  const filterMovies = (e) => {
    setSearchMovies(e.target.value);
  }

  useEffect(() => {
    console.log(searchMovies);

    let newFilteredObj = {};

    for (let key in genresObj) {
      for (let i = 0; i < genresObj[key].length; i++) {
        if (genresObj[key][i].title.toLowerCase().indexOf(searchMovies.toLowerCase()) !== -1) {
          if (newFilteredObj[key]) {
            newFilteredObj[key].push(genresObj[key][i]);
          } else {
            newFilteredObj[key] = [genresObj[key][i]];
          }
        }
      }
    }

    console.log(newFilteredObj);

    setstate(newFilteredObj);

  }, [searchMovies]);

  const openMovieDetails = (data) => {
    navigate("/movieDetails", {
      state: {
        data: data
      }
    });
  }

  return (
    <>
      <div className="container-fluid"><br />
        <Header searchMovies={searchMovies} filterMovies={filterMovies} />
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {Object.keys(state).map((item, index) => {
              return (
                <div key={item} className="row">
                  <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h4>{item}</h4>
                    <div className="row">
                      {state[item].map((item1, index1) => {
                        return (
                          <>
                            <div key={item1.id} className="col-12 col-xs-3 col-sm-3 col-md-3 col-lg-3" onClick={() => openMovieDetails(item1)}>
                              <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                <img className="card-img-top" src={item1.poster} alt="Card image" style={{ width: "100%" }} />
                                <div className="card-body">
                                  <h4 className="card-title">{item1.title}</h4>
                                </div>
                              </div>
                              <br /><br />
                            </div>
                          </>
                        )
                      })}
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </>
  );
}

export default MovieList;
